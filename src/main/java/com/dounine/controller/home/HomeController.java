package com.dounine.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("home")
public class HomeController {
	
	@RequestMapping(value="index.html",method=RequestMethod.GET)
	public String home(){
		return "home/index";
	}
}
