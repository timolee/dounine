package com.dounine.domain.website.floor;

import com.dounine.domain.BaseDomain;

public class FloorCell extends BaseDomain {

	private static final long serialVersionUID = -8330134585729912528L;

	private Integer cell_id;
	private String cell_picture;
	private Boolean cell_ajax;
	private String cell_ajax_url;
	private String cell_url;
	private String cell_ajax_method;
	private Integer cell_column_index;
	private Integer cell_row_index;
	private Integer cell_column_count;
	private Integer cell_row_count;
	private ProductFloor productFloor;
	
	public FloorCell(){
		
	}
	
	public FloorCell(Integer cell_id){
		this.cell_id = cell_id;
	}

	public Integer getCell_id() {
		return cell_id;
	}

	public void setCell_id(Integer cell_id) {
		this.cell_id = cell_id;
	}

	public ProductFloor getProductFloor() {
		return productFloor;
	}

	public String getCell_url() {
		return cell_url;
	}

	public void setCell_url(String cell_url) {
		this.cell_url = cell_url;
	}

	public void setProductFloor(ProductFloor productFloor) {
		this.productFloor = productFloor;
	}

	public String getCell_picture() {
		return cell_picture;
	}

	public void setCell_picture(String cell_picture) {
		this.cell_picture = cell_picture;
	}

	public Boolean getCell_ajax() {
		return cell_ajax;
	}

	public void setCell_ajax(Boolean cell_ajax) {
		this.cell_ajax = cell_ajax;
	}

	public String getCell_ajax_url() {
		return cell_ajax_url;
	}

	public void setCell_ajax_url(String cell_ajax_url) {
		this.cell_ajax_url = cell_ajax_url;
	}

	public String getCell_ajax_method() {
		return cell_ajax_method;
	}

	public void setCell_ajax_method(String cell_ajax_method) {
		this.cell_ajax_method = cell_ajax_method;
	}

	public Integer getCell_column_index() {
		return cell_column_index;
	}

	public void setCell_column_index(Integer cell_column_index) {
		this.cell_column_index = cell_column_index;
	}

	public Integer getCell_row_index() {
		return cell_row_index;
	}

	public void setCell_row_index(Integer cell_row_index) {
		this.cell_row_index = cell_row_index;
	}

	public Integer getCell_column_count() {
		return cell_column_count;
	}

	public void setCell_column_count(Integer cell_column_count) {
		this.cell_column_count = cell_column_count;
	}

	public Integer getCell_row_count() {
		return cell_row_count;
	}

	public void setCell_row_count(Integer cell_row_count) {
		this.cell_row_count = cell_row_count;
	}


}
