package com.dounine.service.website.floor;

import org.springframework.stereotype.Service;

import com.dounine.annoation.Operator;
import com.dounine.domain.website.floor.ProductFloor;
import com.dounine.service.BaseService;

/**
 * @ProjectName:		[ 逗你呢框架管理系统 ]
 * @Package:			[ com.dounine.service.product ]
 * @Author:				[ huanghuanlai ]
 * @CreateTime:			[ 2015年2月24日 上午12:18:51 ]
 * @Copy:				[ dounine.com ]
 * @Version:			[ v1.0 ]
 * @Description:   		[ 产品楼层业务类 ]
 */
@Service
@Operator(name="产品楼层")
public class ProductFloorService extends BaseService<ProductFloor> {
	
}
