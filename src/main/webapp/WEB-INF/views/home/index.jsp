<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>逗你呢(dounine)</title>
		<link rel="shortcut icon" href="${base}/resource/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="${base_url}/resource/home/css/index.css"/>
		<link rel="stylesheet" type="text/css" href="${base_url}/resource/home/css/plugin/dounine-tabs.css"/>
		<link rel="stylesheet" type="text/css" href="${base_url}/resource/home/css/floor.css"/>
		<link rel="stylesheet" type="text/css" href="${base_url}/resource/home/img/icon.css"/>
		<script src="${base_url}/resource/admin/js/common/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${base_url}/resource/admin/js/common/extend/dounine-tools.js" type="text/javascript" charset="utf-8"></script>
		<script src="${base_url}/resource/home/js/plugin/dounine-tabs.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<div id="index-top">
			<div id="i-t-left">
				<div id="i-t-logo">
					<a href="#"><img src="${base_url}/resource/home/img/common/logo.png"/><div>你  呢</div></a>
				</div>
			</div>
			<div id="i-t-right">
				<div id="i-t-r-list">
					<li class="iicon-menu"><a href="#javascript:void(0);"></a></li>
					<li class="index-center-photo"><img src="${base_url}/resource/home/img/common/104479_huanghuanlai.png" /></li>
					<li class="iicon-msg"><a href="#javascript:void(0);"></a></li>
					<li class="iicon-search"><a href="#javascript:void(0);"></a></li>
				</div>
			</div>
		</div>
		
		<div id="index-banner">
			this is index banner.
		</div>
		
		<div id="index-center">
			<div id="i-c-left">
				
				<div id="i-c-dynamic">
					<li class="iicon-dynamic" style="line-height: 48px;"><h4>动 态</h4></li>
					<li class="iicon-download">download count</li>
					<li class="iicon-comment">comment</li>
					<li class="iicon-add" style="border: none;">add</li>
				</div>
				
				<div id="i-c-info">
					<div id="index-tabs" style="margin-top: 10px;">
						<div title="first">你好</div>
						<div title="second">
							asdf
						</div>
					</div>
				</div>
				
			</div>
			<div id="i-c-center">
				<div id="product-floor">
					<div class="floors"></div>
				</div>
			</div>
			<div id="i-c-right">
				
			</div>
		</div>
		
	</body>
</html>

<script type="text/javascript">
	$("#index-tabs").dounine_tabs({
		select : 0
	});
</script>

<script>
	var floor_add_action = '${base}/home/website/homeProductFloor/add.action';
	var floor_edit_action = '${base}/home/website/homeProductFloor/edit.action';
	var floor_all_action = '${base}/home/website/homeProductFloor/all.action';
	var floor_del_action = '${base}/home/website/homeProductFloor/del.action';
	var floor_add_row_action = '${base}/home/website/homeProductFloor/add_row.action';
	var floor_add_column_action = '${base}/home/website/homeProductFloor/add_column.action';
	var floor_clear_action = '${base}/home/website/homeProductFloor/clear.action';
	var floor_del_row_action = '${base}/home/website/homeProductFloor/del_row.action';
	var floor_del_column_action = '${base}/home/website/homeProductFloor/del_column.action';
	
	var floor_cell_all_action = '${base}/home/website/homeFloorCell/all.action';
	var floor_cell_add_action = '${base}/home/website/homeFloorCell/add.action';
	var floor_cell_edit_action = '${base}/home/website/homeFloorCell/edit.action';
	var floor_cell_del_action = '${base}/home/website/homeFloorCell/del.action';
	

	var $floors = null,$floors_list=null,$floor_cell_width=100,$floor_cell_height=100,$floor_init_row=3,$floor_init_cell=10,$floor_click_cells=[],$floor_click_index=null,$floor_cell_operator_div_width=0,$floor_cell_operator_div_height=0;
	
	var content_html = '<div class="floor-list"><div class="floor-info"><div class="floor-info-title"><a href="javascript:void(0);" style="cursor: default;">%{floor_name}</a></div></div><div class="floor-content-values" style="left:%{floor-content-values-left}px;"></div><div class="floor-column"><ul><li style="width:%{floor-cell-width}px;display:none;"></li></ul></div><div class="floor-row"><ul></ul></div><div class="floor-content" style="left:%{floor-content-left}px;"><ul style="top:0px;height:%{floor-content-ul-height}px;"></ul></div></div>';
	
	var floor_button_status = 'create';
	
	$floors = $("div.floors");
	
	$(function(){
		inits_floors();
	});
	
	function inits_floors(){
		$('div.floors').empty();
		$.post(floor_all_action,function(data){
			var list = data;
			for(var ii in list){
				var $item = list[ii];
				
				var _data = content_html.format({
					"floor_name":$item.floor_name,
					"floor-cell-width":$item.floor_cell_width,
					"floor-content-left":$item.floor_cell_width,
					"floor-content-values-left":$item.floor_cell_width,
					"floor-content-ul-height":$item.floor_cell_height
				});
				
				var $floor = $(_data).appendTo($floors);
				var $ul = $floor.find('div.floor-column ul');
				var $ul_lis = $ul.find('li');
				var $content = $floor.find('div.floor-content');
				var $content_title = $floor.find('div.floor-row');
				for(var j =1;j<=$item.floor_row_count;j++){
					$('<li style="display:none;width:'+$item.floor_cell_width+'px;height:'+$item.floor_cell_height+'px;line-height:'+$item.floor_cell_height+'px;">'+j+'</li>').appendTo($content_title.find('ul'));//添加行序号
					if(j<$item.floor_row_count){//此处判断是为了减少多增的一行
						var cont = ['<ul style="top:'+(($item.floor_cell_height*j)+j)+'px;">'];
						cont.push('</ul>');
						$(cont.join('')).appendTo($content);
					}
				}
				for(var i =1;i<=$item.floor_column_count;i++){
					$('<li style="display:none;left:'+(($item.floor_cell_width*i)+i)+'px;width:'+$item.floor_cell_width+'px;">'+$ul.find('li').length+'</li>').appendTo($ul);//添加列序号
					if($item.floor_row_count>0){
						$content.find('ul').each(function(){
							$('<li onclick="floor_cell_click(this);" style="left:'+(($item.floor_cell_width*(i-1))+(i-1))+'px;width:'+$item.floor_cell_width+'px;height:'+$item.floor_cell_height+'px;"></li>').appendTo($(this));//创建单元格
						});
					}
				}
				for(var i in $item.floorCells){
					var _floor_product = $item.floorCells[i];
					
					var min_val_obj_0 = _floor_product.cell_row_index;
					var min_val_obj_1 = _floor_product.cell_column_index; 
					var _add_div_height = parseInt($item.floor_cell_height)*parseInt(_floor_product.cell_row_count);
					var _add_div_width = parseInt($item.floor_cell_width)*parseInt(_floor_product.cell_column_count);
					
					
					var $add_div = $('<div id="floor_cell_info_f'+$floor.index()+'_r'+min_val_obj_0+'_c'+min_val_obj_1+'_rcount'+_floor_product.cell_row_count+'_ccount'+_floor_product.cell_column_count+'" style="background:#ccc;width:'+_add_div_width+'px;height:'+_add_div_height+'px;position: absolute;left:'+((parseInt($item.floor_cell_width)*(min_val_obj_1-1))+parseInt(min_val_obj_1))+'px;top:'+(parseInt($item.floor_cell_height)*(min_val_obj_0-1)+parseInt(min_val_obj_0)-1)+'px;"></div>').appendTo($floor.find("div.floor-content-values"));//添加处于顶层数据
					
					$('<img onclick="floor_img_click_operator(this);" width="'+_add_div_width+'" height="'+_add_div_height+'" src="'+_floor_product.cell_picture+'" />').appendTo($add_div);
					var $form = $('<form onsubmit="return false;"><input type="hidden" name="cell_row_count" /><input type="hidden" name="cell_column_count" /><input type="hidden" name="cell_row_index" /><input type="hidden" name="cell_column_index" /><input type="hidden" name="cell_ajax_method" /><input type="hidden" name="cell_ajax_url" /><input type="hidden" name="cell_ajax" /><input type="hidden" name="cell_id" /><input type="hidden" name="cell_picture" /></form>').appendTo($add_div);
					
					$form.form('load',_floor_product);//绑定数据是单元格中
				}
				
				/*初始化楼层的高度*/
				var row_count = $floor.find('div.floor-row li');
				var column_count = $floor.find('div.floor-column li');
				$floor.height(row_count.length*parseInt($item.floor_cell_height)+row_count.length);
				
				$floor.find('div.floor-content').width((column_count.length-1)*parseInt($item.floor_cell_width)+column_count.length-1);
				$floor.find('div.floor-content').height(row_count.length*parseInt($item.floor_cell_height)+row_count.length-1);
				
				$.ajax({url:floor_cell_all_action,async:false,method:'post',data:{'productFloor.floor_id':$item.floor_id},success:function(data){
					for(var ii in data){
						var oo_b = data[ii];
						var $add_div = $('<div id="floor_cell_info_f'+$floor.index()+'_r'+oo_b.cell_row_index+'_c'+oo_b.cell_column_index+'_rcount'+oo_b.cell_row_count+'_ccount'+oo_b.cell_column_count+'" style="background:#E8E8E8;width:'+($item.floor_cell_width*oo_b.cell_column_count)+'px;height:'+($item.floor_cell_height*oo_b.cell_row_count)+'px;position: absolute;left:'+(($item.floor_cell_width*(oo_b.cell_column_index-1))+oo_b.cell_column_index)+'px;top:'+($item.floor_cell_height*(oo_b.cell_row_index-1)+oo_b.cell_row_index-1)+'px;"></div>').appendTo($floor.find("div.floor-content-values"));//添加处于顶层数据
						
						$('<img click="no" onclick="floor_img_click_operator(this);" width="'+($item.floor_cell_width*oo_b.cell_column_count+oo_b.cell_column_count-1)+'" height="'+($item.floor_cell_height*oo_b.cell_row_count+(oo_b.cell_row_count-1))+'" src="'+oo_b.cell_picture+'" />').appendTo($add_div);
						var $form = $('<form onsubmit="return false;"><input type="hidden" name="cell_url" /><input type="hidden" name="productFloor.floor_id" /><input type="hidden" name="cell_picture" /><input type="hidden" name="cell_id" /><input type="hidden" name="cell_ajax" /><input type="hidden" name="cell_ajax_url" /><input type="hidden" name="cell_ajax_method" /><input type="hidden" name="cell_column_index" /><input type="hidden" name="cell_row_index" /><input type="hidden" name="cell_column_count" /><input type="hidden" name="cell_row_count" /></form>').appendTo($add_div);
						
						//$form.form('load',oo_b);//绑定数据是单元格中
					}
				}});
				
				$floor = null;
			}
		});
	}

	
	var click_floor_row_time = null;
	var click_floor_row_time_y = false;
	function click_floor_row(self){//点击序列号
		click_floor_row_time_y = false;
		var $self = $(self);
		$floor_click_index = $self.parents('div.floor-list').index();
		$floor = get_floor_for_index();
		var $floor_from_obj = $floor.find('div.floor-info-form form').serializeObject();
		clearTimeout(click_floor_row_time);
		click_floor_row_time = setTimeout(function(){
			if(!click_floor_row_time_y){
				$floor.find('div.floor-row-operator').hide();
				$floor.find('div.floor-column').find('li[enter=yes]').attr('enter','no');//清除列
				$floor.find('div.floor-row').find('li[enter=yes]').attr('enter','no');//清除行
			}
		},3000);
		
		if($self.parents('div.floor-column').length==1){
			$floor.find('div.floor-row-operator').css({
				'top':4,
				'left':parseInt($floor_from_obj.floor_cell_width)*$self.index()+$self.index()
			}).show().find('a').text('删列');
			$floor.find('div.floor-column').find('li[enter=yes]').attr('enter','no');//清除列
			$floor.find('div.floor-row').find('li[enter=yes]').attr('enter','no');//清除行
			$self.attr('enter','yes');
		}else if($self.parents('div.floor-row').length==1){
			$floor.find('div.floor-row-operator').css({
				'top':parseInt($floor_from_obj.floor_cell_height)*$self.index()+parseInt($floor_from_obj.floor_cell_height)/2-22/2+$self.index(),
				'left':4
			}).show().find('a').text('删行');
			$floor.find('div.floor-column').find('li[enter=yes]').attr('enter','no');//清除列
			$floor.find('div.floor-row').find('li[enter=yes]').attr('enter','no');//清除行
			$self.attr('enter','yes');
		}
		
	}
	
	
	function get_floor_for_index(){
		return $('div.floor-list').eq($floor_click_index);
	}
	
</script>
